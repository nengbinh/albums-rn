import React from 'react'
import {Text, TouchableOpacity} from 'react-native'

const Button = props => {
    const {buttonStyle, textStyle} = styles;
    const {text, onPress} = props;
    return (
        <TouchableOpacity style={buttonStyle}>
            <Text style={textStyle} onPress={onPress}>{text}</Text>
        </TouchableOpacity>
    )
}

const styles = {
    buttonStyle: {
        padding: 8,
        borderColor: '#007AFF',
        borderRadius: 4,
        borderWidth: 1
    },
    textStyle: {
        fontSize: 14,
        color: '#007AFF',
        marginLeft: 16,
        marginRight: 16
    }
}

export default Button;
