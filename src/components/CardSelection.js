import React from 'react'
import {View} from 'react-native'

const CardSelection = props => {
    return (
        <View style={[styles.containerStyle, props.style]}>
            {props.children}
        </View>
    )
}

const styles = {
    containerStyle: {
        borderBottomWidth: 1,
        borderColor: '#E0E0E0',
        backgroundColor: '#FFF',
        padding: 8,
        justifyContent: 'flex-start',
        flexDirection: 'row',
        position: 'relative'
    }
}

export default CardSelection
