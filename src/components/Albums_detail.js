import React from 'react'
import {View,Text,Image,Linking} from 'react-native'
import Card from './Card'
import CardSelection from './CardSelection'
import Button from './Button'

const Albums_detail = props => {
    const {image, thumbnail, title, description, link} = props.album;
    const {
        imageContainerStyle,
        imageStyle,
        thumbnailStyle,
        infoContainerStyle,
        titleStyle,
        descriptionStyle,
        buttonContainerStyle
    } = styles;
    //
    return (
        <Card>
            <CardSelection style={imageContainerStyle}>
                <Image source={{uri: image}} style={imageStyle} />
            </CardSelection>
            <CardSelection>
                <Image source={{url: thumbnail}} style={thumbnailStyle}/>
                <View style={infoContainerStyle}>
                    <Text style={titleStyle}>{title}</Text>
                    <Text style={descriptionStyle}>{description}</Text>
                </View>
            </CardSelection>
            <CardSelection style={buttonContainerStyle}>
                <Button text='Buy' onPress={()=> Linking.openURL(link)} />
            </CardSelection>
        </Card>
    )
};

const styles = {
    imageStyle: {
        flex: 1,
        height: 300
    },
    imageContainerStyle: {
        padding: 0,
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        overflow: 'hidden'
    },
    thumbnailStyle: {
        width: 60,
        height: 60,
        borderRadius: 2,
        marginRight: 8
    },
    titleStyle: {
        fontSize: 18,
        fontWeight: '600',
        marginBottom: 6
    },
    descriptionStyle: {
        fontSize: 13
    },
    infoContainerStyle: {
        flex: 1,
        marginBottom: 6
    },
    buttonContainerStyle: {
        justifyContent: 'center',
        alignItems: 'center'
    }
};

export default Albums_detail
