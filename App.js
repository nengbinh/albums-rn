import React from 'react';
import { StyleSheet, Text,View } from 'react-native';
import Header from './src/components/Header'
import AlbumsList from './src/components/Albums_list'

export default class App extends React.Component {

  render() {
    return (
      <View style={{flex: 1}}>
        <Header text='Wiz Khalifa' />
        <AlbumsList />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
